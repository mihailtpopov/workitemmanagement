﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;

namespace WIMTests.FakeModelsForCommandsTests
{
    public class FakeBoard : IBoard
    {
        private List<IWorkItem> workItems = new List<IWorkItem>();
        public string Name => "testBoard";

        public EventLog EventLog { get; } = new EventLog();

        public IReadOnlyList<IWorkItem> WorkItems { get; } = new List<IWorkItem>();

        public int ID => throw new NotImplementedException();

        public void AddWorkItem(IWorkItem item)
        {
            if (this.workItems.Contains(item))
            {
                throw new ArgumentException(ErrorStrings.DUPLICATE_WORK_ITEM);
            }

            workItems.Add(item);

            this.EventLog.AddEvent(
            new Event(string.Format(EventStrings.WORKITEM_ADDED_TO_BOARD, item.GetType().Name, item.Title)));
        }

        public void CheckWorkItemExistance(string type, string title)
        {
            bool doesExist = type.ToLower() switch
            {
                "bug" => this.WorkItems
                .Where(item => item is IBug)
                .FirstOrDefault(item => item.Title == title) != null ? true : false,

                "feedback" => this.WorkItems
                .Where(item => item is IFeedback)
                .FirstOrDefault(item => item.Title == title) != null ? true : false,

                "story" => this.WorkItems
                .Where(item => item is IStory)
                .FirstOrDefault(item => item.Title == title) != null ? true : false,

                _ => throw new ArgumentException(ErrorStrings.INVALID_WORK_ITEM_TYPE)
            };

            if (doesExist)
            {
                throw type.ToLower() switch
                {
                    "bug" => new ArgumentException(string.Format(ErrorStrings.DUPLICATE_WORK_ITEM, "Bug", this.Name)),
                    "feedback" => new ArgumentException(string.Format(ErrorStrings.DUPLICATE_WORK_ITEM, "Feedback", this.Name)),
                    "story" => new ArgumentException(string.Format(ErrorStrings.DUPLICATE_WORK_ITEM, "Story", this.Name)),
                    _ => new ArgumentException(),
                };
            }
        }

        public IEnumerable<IBug> GetAllBugs()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IFeedback> GetAllFeedbacks()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IStory> GetAllStories()
        {
            throw new NotImplementedException();
        }

        public IBug GetBugByTitle(string bugTitle)
        {
            throw new NotImplementedException();
        }

        public IFeedback GetFeedbackByTitle(string feedbackTitle)
        {
            throw new NotImplementedException();
        }

        public IStory GetStoryByTitle(string storyTitle)
        {
            throw new NotImplementedException();
        }

        public IWorkItem GetWorkItemByTypeAndTitle(string type, string title)
        {
            return type.ToLower() switch
            {
                "bug" => this.WorkItems.Where(item => (item is IBug && item.Title == title)).FirstOrDefault(),
                "feedback" => this.WorkItems.Where(item => (item is IFeedback && item.Title == title)).FirstOrDefault(),
                "story" => this.WorkItems.Where(item => (item is IStory && item.Title == title)).FirstOrDefault(),
                _ => throw new ArgumentException(string.Format(ErrorStrings.INVALID_WORK_ITEM_TYPE))
            }
            ?? throw new ArgumentException(string.Format(
                ErrorStrings.INVALID_WORK_ITEM,
                type[0].ToString().ToUpper() + type.Substring(1),
                title));
        }

        public string ListAllItems()
        {
            throw new NotImplementedException();
        }

        public string ShowActivity()
        {
            throw new NotImplementedException();
        }
    }
}
