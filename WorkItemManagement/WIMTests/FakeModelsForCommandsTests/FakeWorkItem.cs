﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;

namespace WIMTests.FakeModelsForCommandsTests
{
    public class FakeWorkItem : IWorkItem
    {
        public int ID => throw new NotImplementedException();

        public string Title => "testTitle";

        public string Description => throw new NotImplementedException();

        public IReadOnlyList<Comment> Comments => throw new NotImplementedException();

        public IEventLog EventLog => throw new NotImplementedException();

        public void AddComment(Comment comment)
        {
            throw new NotImplementedException();
        }

        public void RecordTeamAndBoard(ITeam team, IBoard board)
        {
            throw new NotImplementedException();
        }
    }
}
