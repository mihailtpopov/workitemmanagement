﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Commands.CreateWorkItem;
using WorkItemManagement.Commands.CreateWorkItemInBoard;

namespace WIMTests.CommandsTests.CreateWorkItemTests
{
    [TestClass]
    public class CreateFeedbackInBoardCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_ParamsNumberInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>() { "teamName", "boardName", "lastButNotEnough" };

            //Act & Assert
             new CreateFeedbackInBoardCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team with such name does not exists")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "feedDescription"
            };

            //Act & Assert
            new CreateFeedbackInBoardCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Board with this name does not exists in this team")]
        public void ThrowWhen_BoardDoesNotExist()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "boardName",
                "feedTitle",
                "feedDescription"
            };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());

            //Assert
            new CreateBugInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Feedback with such title already exists in this team board")]
        public void ThrowWhen_FeedbackTitleExists()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "feedDescription"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);
            fakeDb.AddTeam(fakeTeam);
            fakeBoard.AddWorkItem(new FakeWorkItem());

            //Assert
            new CreateBugInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }
    }
}
