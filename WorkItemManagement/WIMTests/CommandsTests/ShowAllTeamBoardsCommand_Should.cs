﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core;
using static WIMTests.CommandsTests.CreateNewTeamCommand_Should;
using WIMTests.FakeModelsForCommandsTests;

namespace WIMTests.CommandsTests
{
    [TestClass]
    public class ShowAllTeamBoardsCommand_Should
    {

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid params number")]
        public void ThrowWhen_InvalidParameteresNumber()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam" , "wrongParam" };

            //Act & Assert
            new ShowAllTeamBoardsCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No boards in team")]
        public void ThrowWhen_NoBoardsInTeam()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam"};

            //Act
           //new CreateTeamCommand(commandParams).Execute();

            //Assert
            new ShowAllTeamBoardsCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team does not exist")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam" };

            //Act & Assert
            new ShowAllTeamBoardsCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }
    }
}
