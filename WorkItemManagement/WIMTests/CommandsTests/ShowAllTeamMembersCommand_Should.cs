﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using static WIMTests.CommandsTests.CreateNewTeamCommand_Should;

namespace WIMTests.CommandsTests
{
    [TestClass]
    public class ShowAllTeamMembersCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_InvalidParamsNumber()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam", "wrongParam" };

            //Act & Assert
            new ShowAllTeamMembersCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team does not exist")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam" };

            //Act & Assert
            new ShowAllTeamMembersCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No team members")]
        public void ThrowWhen_NoMembersInTeam()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam" };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());

            //Act & Assert
            new ShowAllTeamMembersCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }
    }
}
