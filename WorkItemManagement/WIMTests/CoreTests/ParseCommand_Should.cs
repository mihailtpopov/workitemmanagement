﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;

namespace WIMTests.CoreTests
{
    [TestClass]
    public class ParseCommand_Should
    {
        [TestMethod]
        public void ThrowWhenInvalidCommand()
        {
            // Arrange
            string invalidCommand = "invalidCommand";
            var cmdManager = new CommandManager();

            // Act & Assert
            Assert.ThrowsException<InvalidOperationException>(() => cmdManager.ParseCommand(invalidCommand, new FakeDb(), new FakeWriter()));
        }

        [TestMethod]
        [DataRow("createmember")]
        [DataRow("createbug")]
        [DataRow("createfeedback")]
        public void ThrowWhenValidCommand(string validCommand)
        {
            // Arrange
            var cmdManager = new CommandManager();

            // Act
            var cmd = cmdManager.ParseCommand(validCommand, new FakeDb(), new FakeWriter());

            // Assert
            Assert.IsInstanceOfType(cmd, typeof(Command));
        }

    }
}
