﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    class CheckBoardExistence_Should
    {
        [TestMethod]
        public void ThrowWhen_BoardDoesNotExist()
        {
            FakeTeam fakeTeam = new FakeTeam();

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.CheckBoardExistance("nonExistingBoardName"));
        }

        class FakeTeam : ITeam
        {
            public string Name => throw new NotImplementedException();

            public IReadOnlyList<IMember> Members => throw new NotImplementedException();

            public IReadOnlyList<IBoard> Boards { get; } = new List<IBoard>();

            public IEventLog EventLog => throw new NotImplementedException();

            public int ID => throw new NotImplementedException();

            public void AddBoard(IBoard board)
            {
                throw new NotImplementedException();
            }

            public void AddTeammate(IMember member)
            {
                throw new NotImplementedException();
            }

            public void CheckBoardExistance(string boardName)
            {
                throw new NotImplementedException();
            }

            public void CheckMemberExistance(string memberName)
            {
                throw new NotImplementedException();
            }

            public IBoard GetBoard(string boardName)
            {
                throw new NotImplementedException();
            }

            public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public IMember GetMember(string memberName)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }

            public string ShowBoards()
            {
                throw new NotImplementedException();
            }

            public string ShowMembers()
            {
                throw new NotImplementedException();
            }
        }

        class FakeBoard : IBoard
        {
            public string Name { get; }
            public EventLog EventLog => throw new NotImplementedException();

            public IReadOnlyList<IWorkItem> WorkItems => throw new NotImplementedException();

            public int ID => throw new NotImplementedException();

            public void AddWorkItem(IWorkItem item)
            {
                throw new NotImplementedException();
            }

            public void CheckWorkItemExistance(string type, string title)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetAllBugs()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IFeedback> GetAllFeedbacks()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetAllStories()
            {
                throw new NotImplementedException();
            }

            public IBug GetBugByTitle(string bugTitle)
            {
                throw new NotImplementedException();
            }

            public IFeedback GetFeedbackByTitle(string feedbackTitle)
            {
                throw new NotImplementedException();
            }

            public IStory GetStoryByTitle(string storyTitle)
            {
                throw new NotImplementedException();
            }

            public IWorkItem GetWorkItemByTypeAndTitle(string type, string title)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }
        }
    }
}
