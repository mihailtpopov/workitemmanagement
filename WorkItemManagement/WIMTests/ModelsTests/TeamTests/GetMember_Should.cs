﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    public class GetMember_Should
    {
        [TestMethod]
        public void ThrowsWhen_MemberDoesNotExists()
        {
            var fakeTeam = new FakeTeam();

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.GetMember("nonExistingMemberName"));
        }

        [TestMethod]
        public void GetsMemberCorrectly()
        {
            //Arrange
            var fakeTeam = new FakeTeam();
            var fakeMember = new FakeMember();

            //Act
            fakeTeam.AddTeammate(fakeMember);

            //Assert
            Assert.IsInstanceOfType(fakeTeam.GetMember(fakeMember.Name), typeof(IMember));
        }
        class FakeTeam : ITeam
        {
            private List<IMember> members = new List<IMember>();
            public string Name => "testName";

            public IReadOnlyList<IMember> Members { get => this.members; }

            public IReadOnlyList<IBoard> Boards => throw new NotImplementedException();

            public IEventLog EventLog { get; } = new EventLog();

            public int ID => throw new NotImplementedException();

            public void AddBoard(IBoard board)
            {
                throw new NotImplementedException();
            }

            public void AddTeammate(IMember member)
            {
                if (this.members.Contains(member))
                {
                    throw new ArgumentException(string.Format(ErrorStrings.MEMBER_EXIST, member.Name, this.Name));
                }

                this.members.Add(member);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADDED, member.Name)));
            }

            public void CheckBoardExistance(string boardName)
            {
                throw new NotImplementedException();
            }

            public void CheckMemberExistance(string memberName)
            {
                throw new NotImplementedException();
            }

            public IBoard GetBoard(string boardName)
            {
                throw new NotImplementedException();
            }

            public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public IMember GetMember(string memberName)
            {
                var member = this.Members
               .Where(member => member.Name == memberName)
               .FirstOrDefault() ??
               throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_MEMBER, memberName, this.Name));

                return member;
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }

            public string ShowBoards()
            {
                throw new NotImplementedException();
            }

            public string ShowMembers()
            {
                throw new NotImplementedException();
            }
        }

        class FakeMember : IMember
        {
            public int ID => throw new NotImplementedException();

            public string Name => "testMember";

            public IReadOnlyList<ITeam> Teams => throw new NotImplementedException();

            public IEventLog EventLog => throw new NotImplementedException();

            public IReadOnlyList<IWorkItem> WorkItems => throw new NotImplementedException();

            public void AddTeam(Team team)
            {
                throw new NotImplementedException();
            }

            public void AssignItem(IBugStory item)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetAllBugs()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetAllStories()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
            {
                throw new NotImplementedException();
            }

            public void UnassignItem(IBugStory item)
            {
                throw new NotImplementedException();
            }
        }
    }


}
