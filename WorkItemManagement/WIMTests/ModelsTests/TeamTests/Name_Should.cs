﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    public class Name_Should
    {
        [TestMethod]
        public void Throw_When_NameIsNull()
        {
            //Arrange
            string name = "Billy";

            //Act
            Team team = new Team(name);

            //Assert
            Assert.ThrowsException<ArgumentException>(()=>team.Name=null);
        }

        public void Throw_When_NameIsOverMaxLength()
        {
            //Arrange
            string name = "theinvincible";

            //Act
            Team team = new Team(name);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => team.Name = new string('a',21));
        }

        public void Throw_When_NameIsUnderMaxLength()
        {
            //Arrange
            string name = "theinvincible";

            //Act
            Team team = new Team(name);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => team.Name = new string('a', 1));
        }
    }
}
