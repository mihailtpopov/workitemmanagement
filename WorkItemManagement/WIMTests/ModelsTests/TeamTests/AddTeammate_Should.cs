﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    public class AddTeammate_Should
    {
        [TestMethod]
        public void ThrowWhen_MemberAlreadyAdded()
        {
            //Arrange
            string teamName = "Test name";
            var team = new Team(teamName);
            string memberName = "Billy";
            var member = new Member(memberName);

            //Act
            team.AddTeammate(member);

            //Assert
            Assert.ThrowsException<ArgumentException>(() => team.AddTeammate(member));
        }

        [TestMethod]
        public void AddNewEventInEventLog()
        {
            //Arrange
            string teamName = "Test name";
            var team = new Team(teamName);
            string memberName = "Billy";
            var member = new Member(memberName);

            //Act
            team.AddTeammate(member);

            //Assert
            Assert.IsTrue(team.EventLog.History.Count == 2);
        }

        [TestMethod]
        public void AddMemberInTeam()
        {
            //Arrange
            string teamName = "Test name";
            var team = new Team(teamName);
            string memberName = "Billy";
            var member = new Member(memberName);

            //Act
            team.AddTeammate(member);

            //Assert
            Assert.IsTrue(team.Members.Count==1);
        }

        [TestMethod]
        public void AddTeamInMemberTeams()
        {
            //Arrange
            string teamName = "Test name";
            var team = new Team(teamName);
            string memberName = "Billy";
            var member = new Member(memberName);

            //Act
            team.AddTeammate(member);

            //Assert
            Assert.IsTrue(member.Teams.Count==1);
        }
    }
}
