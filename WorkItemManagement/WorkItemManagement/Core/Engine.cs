﻿using System;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core
{
    public class Engine : IEngine
    {
        private static Engine instance;

        public static IEngine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Engine();
                }

                return instance;
            }
        }

        private readonly IWriter writer;
        private readonly IReader reader;
        private readonly Processor processor;
        private readonly IDatabase database;

        private Engine()
        {
            this.writer = new ConsoleWriter();
            this.reader = new ConsoleReader();
            this.processor = new Processor();
            this.database = Database.Instance;
        }

        public void Run()
        {
            while (true)
            {
                Console.ResetColor();
                // Read -> Process -> Print -> Repeat
                string input = this.reader.Read();

                switch (input)
                {
                    case "": continue;
                    case "exit": goto End;
                    default:
                        {
                            string result = this.processor.Process(input, database, writer);
                            this.writer.Print(result);
                        }
                        break;
                }
            }
        End:;
        }
    }
}
