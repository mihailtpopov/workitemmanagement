﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Core
{
    public static class ErrorStrings
    {
        //team errors
        public const string INVALID_TEAM_NAME = "Sorry, we can not find team {0}.";
        public const string INVALID_TEAM_BOARD = "Sorry, we can not find board {0} in team {1}.";
        public const string INVALID_TEAM_MEMBER = "Sorry, we can not find member {0} in team {1}.";
        public const string NO_TEAM_MEMBERS_YET = "There are still no members in this team.";
        public const string NO_TEAM_BOARDS_YET = "There are still no boards in this team.";
        public const string NO_TEAMS_YET = "There are still no teams created.";
        public const string NO_WORKITEMS_IN_TEAM_BOARDS_YET = "There are still no work items in this team board.";
        public const string NO_WORKITEMS_IN_TEAM_YET = "There are still no work items in this team.";
        
        //bug errors
        public const string INVALID_BUG_SEVERITY = "Bug severity should be one of the following: critical, major, minor.";
        public const string INVALID_BUG_STATUS = "Bug status should be one of the following: active, fixed.";
        public const string BUGS_WITH_STATUS_NOT_FOUND = "There are no bugs assigned to this assignee with status {0}";

        //member reports
        public const string MEMBER_EXIST = "Member with name {0} exist in {1}. If you want to add a new member please enter unique name for the team.";
        public const string MEMBER_ALREADY_IN_TEAM = "Member already resides in this team.";
        public const string NO_MEMBERS_YET = "There are still no registered members.";
        public const string ALREADY_ASSIGNED_ITEM = "This work item is already assigned to {0}";
        public const string NO_SUCH_ITEM_ASSIGNED = "This work item is not assigned to {0}";
        public const string FEEDBACK_CANT_BE_ASSIGNED = "Feedback can't be assigned to any member";
        public const string INVALID_UNASSIGN_OPERATION = "Feedback can't be assigned nor unassigned to any member";

        //feedback errors
        public const string INVALID_FEEDBACK_STATUS = "Feedback status should be one of the following: new, unscheduled, scheduled, done.";
        public const string INVALID_RATING = "Rating can't be negative.";
        public const string FEEDBACK_WITH_STATUS_NOT_FOUND = "There are no feedbacks assigned to this assignee with status {0}";

        //story errors
        public const string INVALID_STORY_STATUS = "Story status should be one of the following: not done, in progress, done.";
        public const string INVALID_STORY_SIZE = "Story size should be one of the following: large, medium, small.";
        public const string INVALID_STORY_TITLE = "Story with title \"{0}\" does not resides in this board.";
        public const string STORIES_WITH_STATUS_NOT_FOUND = "There are no stories assigned to this assignee with status {0}";

        //board errors
        public const string DUPLICATE_WORK_ITEM = "{0} with such title already exist in {1}.";
        public const string NO_WORK_ITEMS_IN_BOARD = "There are still no {0} in this board";

        //comment errors
        public const string CAN_NOT_BE_NULL = "{0} can not be null.";

        //command errors
        public const string INVALID_PARAMETERS_NUMBER = "Invalid number of command parameters. Please enter {0}.";
        public const string OBJECT_DOES_NOT_EXIST = "{0} {1} does not exist.";
        public const string DUPLICATE_OBJECT = "{0} {1} already exist. Please enter unique name.";
        public const string INVALID_SORTING_KEY = "Invalid sorting key.Please eneter as first parameter one of the following: {0}.";

        //common errors
        public const string INVALID_PRIORITY = "Priority should be one of the following: high, medium, low.";
        public const string INVALID_NAME_LENGTH = "{0} name must be between {1} and {2} symbols.";
        public const string UNEXPECTED_ERROR = "Unexpected error occured! Please try again in a few seconds.";
        public const string INVALID_WORK_ITEM = "{0} with title \"{1}\" does not exist. Please provide an existing title.";
        public const string INVALID_WORK_ITEM_TYPE = "Work item should be one of the following: bug, story, feedback.";
        public const string INVALID_WORK_ITEM_LABEL = "{0} {1} should be one of the following: {2}.";
        public const string INVALID_RATING_TYPE = "Rating should be a positive whole number.";
        public const string NOT_ASSIGNED_WORKITEMS = "No {0} are assigned to this assignee.";
        public const string NO_WORK_ITEMS_YET = "There are still no {0} created."; //*

        //filtration errors
        public const string INVALID_FILTRATION = "No results available.";
        public const string INVALID_FILTRATION_PARAM = "You can filtrate by {0} only {1}.";
    }
}
