﻿using System.Collections.Generic;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov, Georgi Stefanov</author>
    public interface IFactory
    {
        Member CreateMember(string  name);

        Team CreateTeam(string name);

        IWorkItem CreateBugInBoard(string title, string description, Priority priority, BugSeverity severity, List<string> stepsToReproduce);

        IWorkItem CreateFeedbackInBoard(string title, string description);

        IWorkItem CreateStoryInBoard(string title, string description, Priority priority, StorySize size);

        Comment CreateComment(string commentText, IMember author);

        Board CreateBoard(string name);

    }
}
