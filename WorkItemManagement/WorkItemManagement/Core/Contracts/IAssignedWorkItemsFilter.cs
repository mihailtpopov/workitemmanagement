﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov</author>
    public interface IAssignedWorkItemsFilter
    {
        public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status);

        public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status);
    }
}
