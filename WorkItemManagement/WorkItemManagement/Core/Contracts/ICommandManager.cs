﻿using WorkItemManagement.Commands.Contracts;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov, Georgi Stefanov</author>
    public interface ICommandManager
    {
        /// <summary>
        /// Parses the command line arguments into array.
        /// Gets the first argument as the command name and the remaining as the command prameters.
        /// Returns a command object based on the command name or throws exception if can not recognise the command name. 
        /// </summary>
        /// <param name="commandLine">The command parameters obtained after parsing</param>
        /// <param name="dabatase">The Database object</param>
        /// <param name="writer">The Writer object</param>
        /// <returns>Returns a command object based on the command name or throws exception if can not recognise the command name.</returns>
        ICommand ParseCommand(string commandLine, IDatabase dabatase, IWriter writer);
    }
}
