﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov</author>
    public interface IWorkItemsFilter : IAssignedWorkItemsFilter
    {
        public IEnumerable<IFeedback> GetWorkItemsFilteredBy(FeedbackStatus status);
    }
}
