﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Models
{
    /// <author>Mihail Popov</author>
    public class EventLog : IEventLog
    {
        private List<Event> history;

        public EventLog()
        {
            history = new List<Event>();
        }

        public IReadOnlyList<Event> History { get => this.history; }

        public void AddEvent(Event newEvent)
        {
            history.Add(newEvent);
        }
    }
}
