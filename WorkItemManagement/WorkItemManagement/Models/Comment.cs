﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Models
{
    /// <author>Georgi Stefanov</author>
    public class Comment
    {

        public Comment(string message, IMember author)
        {
            this.Message = message;

            if (author==null)
            {
                throw new ArgumentException(string.Format(ErrorStrings.CAN_NOT_BE_NULL, "Author"));
            }
            this.Author = author;
        }

        public string Message { get; private set; }

        public IMember Author { get; }
    }
}
