﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Enums
{
    public enum BugStatus
    {
        Active,
        Fixed
    }
}
