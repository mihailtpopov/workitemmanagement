﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Georgi Stefanov</author>
    public interface IBug : IWorkItem, IBugStory
    {
        BugStatus Status { get; }

        IReadOnlyList<string> StepsToReproduce { get; }

        BugSeverity Severity { get; }

        /// <summary>
        /// Changes the bug priority and writes into EvenetLog
        /// </summary>
        /// <param name="priority">The new priority</param>
        void ChangePriority(Priority priority);

        /// <summary>
        /// Changes the bug severity and writes into EvenetLog
        /// </summary>
        /// <param name="severity">The new severity</param>
        void ChangeSeverity(BugSeverity severity);

        /// <summary>
        /// Changes the bug status and writes into EvenetLog
        /// </summary>
        /// <param name="status">The new status</param>
        void ChangeStatus(BugStatus status);

    }
}
