﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Georgi Stefanov</author>
    public interface IFeedback : IWorkItem
    {
        public FeedbackStatus Status { get; }

        public int Rating { get; }

        /// <summary>
        /// Changes the feedback status and writes into EvenetLog
        /// </summary>
        /// <param name="status">The new priority</param>
        public void ChangeStatus(FeedbackStatus status);

        /// <summary>
        /// Changes the feedback rating and writes into EvenetLog
        /// </summary>
        /// <param name="status">The new rating</param>
        public void ChangeRating(int rating);


    }
}
