﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Mihail Popov</author>
    class ListAllBoardItemsCommand : Command
    {
        public ListAllBoardItemsCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if(this.CommandParameters.Count!=2)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team and board"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);

            if(board.WorkItems.Count == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "work items"));
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of work items in board \"{board.Name}\" in team \"{team.Name}\":");
            sb.Append(board.ListAllItems());

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
