﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Mihail Popov</author>
    class ListAllTeamWorkItemsCommand : Command
    {
        public ListAllTeamWorkItemsCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
           if(this.CommandParameters.Count!=1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of work items in team \"{team.Name}\":");
            sb.Append(string.Join(Environment.NewLine, team.ListAllItems()));

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
