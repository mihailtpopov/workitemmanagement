﻿using WorkItemManagement.Core.Contracts;
using System.Collections.Generic;
using WorkItemManagement.Commands.Contracts;
using WorkItemManagement.Core;

namespace WorkItemManagement.Commands.Abstracts
{
    public abstract class Command : ICommand
    {
        protected Command(IList<string> commandParameters, IDatabase database, IWriter writer)
        {
            this.CommandParameters = new List<string>(commandParameters);
            this.Database = database;
            this.Writer = writer;
        }

        public abstract string Execute();

        protected IList<string> CommandParameters
        {
            get;
        }

        protected IDatabase Database
        {
            get;
        }

        protected IFactory Factory
        {
            get
            {
                return Core.Factory.Instance;
            }
        }

        protected IWriter Writer { get; set; }
    }
}
