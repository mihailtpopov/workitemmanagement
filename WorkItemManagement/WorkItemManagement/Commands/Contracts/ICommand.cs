﻿using System.Collections.Generic;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Commands.Contracts
{
    public interface ICommand
    {
        string Execute();
    }
}
