﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ChangeStoryCommands
{
    /// <author>Mihail Popov</author>
    public class ChangeStoryPriorityCommand : Command
    {
        public ChangeStoryPriorityCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, story title and the new priority"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var story = board.GetStoryByTitle(this.CommandParameters[2]);
            
            if(!Enum.TryParse(this.CommandParameters[3], out Priority newPriority))
            {
                throw new ArgumentException(ErrorStrings.INVALID_PRIORITY);
            }

            if(story.Priority == newPriority)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Priority", story.Priority);
                //throw new ArgumentException(string.Format(Strings.PROPERTY_NOT_CHANGED, "Priority", story.Priority));
            }

            Priority current = story.Priority;

            story.ChangePriority(newPriority);

            board.EventLog.AddEvent(new Event(
                string.Format(EventStrings.WI_PROPERTY_CHANGED, "Story", story.Title, "priority", current, story.Priority)));

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(SuccessfullStrings.WORK_ITEM_CHANGED, "Story", $"{story.Title}", "priority", newPriority);
        }

    }
}
