﻿
using System;
using System.Collections.Generic;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ChangeStoryCommands
{
    /// <author>Mihail Popov</author>
   public class ChangeStorySizeCommand : Command
    {
        public ChangeStorySizeCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, story title and the new size"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var story = board.GetStoryByTitle(this.CommandParameters[2]);

            if(!Enum.TryParse(this.CommandParameters[3], out StorySize newSize))
            {
                throw new ArgumentException(ErrorStrings.INVALID_STORY_SIZE);
            }

            if(story.Size == newSize)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Size", story.Size);
                //throw new ArgumentException(string.Format(Strings.PROPERTY_NOT_CHANGED, "Size", story.Size));
            }

            StorySize current = story.Size;
  
            story.ChangeSize(newSize);

            board.EventLog.AddEvent(new Event(
                string.Format(EventStrings.WI_PROPERTY_CHANGED, story.Title, "Story", "size", current, story.Size)));

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(SuccessfullStrings.WORK_ITEM_CHANGED, "Story", $"{story.Title}", "size", newSize);
        }
    }
}
