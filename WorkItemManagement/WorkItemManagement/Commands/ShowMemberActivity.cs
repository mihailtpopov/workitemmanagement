﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    class ShowMemberActivity : Command
    {
        public ShowMemberActivity(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }
        public override string Execute()
        {
            // COMMAND SINTAXIS: showpersonctivity Person
            if (this.CommandParameters.Count != 1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "name"));
            }

            IMember member = this.Database.GetMember(this.CommandParameters[0]);

            StringBuilder memberHistory = new StringBuilder();

            memberHistory.AppendLine($"Member {member.Name} activity:");
            memberHistory.Append(
                string.Join(Environment.NewLine,member.EventLog.History.ToList()));

            this.Writer.SetPrintColor(StringType.Activity);
            return memberHistory.ToString().Trim();
        }
    }
}
