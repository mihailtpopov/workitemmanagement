﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    class ShowBoardActivity : Command
    {
        public ShowBoardActivity(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: showboardactivity Team Board
            if (this.CommandParameters.Count != 2)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name and board name"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            var board = team.GetBoard(this.CommandParameters[1]);

            StringBuilder boardHistory = new StringBuilder();

            boardHistory.AppendLine($"Board \"{board}\" in team \"{team}\" activity:");
            boardHistory.Append(board.ShowActivity());

            this.Writer.SetPrintColor(StringType.Activity);
            return boardHistory.ToString().Trim();
        }
    }
}
